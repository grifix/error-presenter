<?php

declare(strict_types=1);

namespace Grifix\ErrorPresenter;

final class ExceptionConverter implements ExceptionConverterInterface
{
    /** @var callable|string|null|mixed[] */
    private $errorContent;

    /**
     * @param callable|string|null|mixed[] $errorContent
     */
    public function __construct(
        private readonly string $exceptionPattern,
        private readonly ?int $httpCode = null,
        private readonly ?int $errorCode = null,
        string|callable|null|array $errorContent = null,
    ) {
        $this->errorContent = $errorContent;
    }

    public static function create(
        string $exceptionPattern,
        ?int $httpCode = null,
        ?int $errorCode = null,
        string|callable|null $errorMessage = null,
    ): self {
        return new self($exceptionPattern, $httpCode, $errorCode, $errorMessage);
    }

    private function isPatternRegexp(): bool
    {
        return str_starts_with($this->exceptionPattern, '/');
    }

    public function match(\Throwable $exception): bool
    {
        if ($this->isPatternRegexp()) {
            return (bool)preg_match($this->exceptionPattern, $exception::class);
        }

        return str_starts_with($exception::class, $this->exceptionPattern);
    }

    public function convert(\Throwable $exception): Error
    {
        return new Error(
            $this->createErrorContent($exception),
            $this->httpCode ?? 400,
        );
    }

    /**
     * @return mixed[]
     */
    private function createErrorContent(\Throwable $exception): array
    {
        if (is_string($this->errorContent)) {
            return [
                'error' => [
                    'message' => $this->errorContent,
                    'code' => $this->errorCode ?? $exception->getCode(),
                ]
            ];
        }
        if (is_callable($this->errorContent)) {
            $func = $this->errorContent;

            return $func($exception);
        }
        if ($exception->getMessage()) {
            return [
                'error' => [
                    'message' => $exception->getMessage(),
                    'code' => $this->errorCode ?? $exception->getCode(),
                ]
            ];
        }

        return [
            'error' => [
                'message' => 'Unknown error',
                'code' => 0,
            ]
        ];
    }
}

<?php

declare(strict_types=1);

namespace Grifix\ErrorPresenter;

interface ErrorPresenterInterface
{
    public function present(\Throwable $exception): ?Error;
}

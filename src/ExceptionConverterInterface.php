<?php

declare(strict_types=1);

namespace Grifix\ErrorPresenter;

interface ExceptionConverterInterface
{
    public function match(\Throwable $exception): bool;

    public function convert(\Throwable $exception): Error;
}

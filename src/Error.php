<?php

declare(strict_types=1);

namespace Grifix\ErrorPresenter;

final readonly class Error
{
    /**
     * @param mixed[] $content
     */
    public function __construct(
        public array $content,
        public int $httpCode,
    ) {
    }
}

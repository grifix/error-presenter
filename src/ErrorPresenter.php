<?php

declare(strict_types=1);

namespace Grifix\ErrorPresenter;

final class ErrorPresenter implements ErrorPresenterInterface
{
    /** @var ExceptionConverterInterface[] */
    private readonly array $converters;

    public function __construct(ExceptionConverterInterface ...$converters)
    {
        $this->converters = $converters;
    }

    public static function create(ExceptionConverterInterface ...$converters): self
    {
        return new self(...$converters);
    }

    public function present(\Throwable $exception): Error
    {
        foreach ($this->converters as $converter) {
            if ($converter->match($exception)) {
                return $converter->convert($exception);
            }
        }

        return new Error(
            [
                'error' =>
                    [
                        'message' => 'Server error',
                        'code' => 0,
                    ]
            ],
            500
        );
    }
}

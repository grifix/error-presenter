<?php

declare(strict_types=1);

namespace Grifix\ErrorPresenter\Tests;

use Grifix\ErrorPresenter\Error;
use Grifix\ErrorPresenter\ErrorPresenter;
use Grifix\ErrorPresenter\ExceptionConverter;
use Grifix\ErrorPresenter\ExceptionConverterInterface;
use PHPUnit\Framework\TestCase;

final class ErrorPresenterTest extends TestCase
{
    /**
     * @param ExceptionConverterInterface[] $converters
     *
     * @dataProvider dataProvider
     */
    public function testItPresents(array $converters, \Throwable $exception, ?Error $expectedError): void
    {
        self::assertEquals($expectedError, ErrorPresenter::create(...$converters)->present($exception));
    }

    /**
     * @return mixed[]
     */
    public function dataProvider(): array
    {
        return [
            'default' => [
                [
                    ExceptionConverter::create(\Exception::class),
                ],
                new \Exception(),
                new Error(
                    [
                        'error' =>
                            [
                                'message' => 'Unknown error',
                                'code' => 0,
                            ],
                    ],
                    400
                ),
            ],
            'message from exception' => [
                [
                    ExceptionConverter::create(\Exception::class),
                ],
                new \Exception('Exception message'),
                new Error(
                    [
                        'error' =>
                            [
                                'message' => 'Exception message',
                                'code' => 0,
                            ],
                    ],
                    400
                ),
            ],
            'message from converter' => [
                [
                    ExceptionConverter::create(
                        \Exception::class,
                        errorMessage: 'Converter message'
                    ),
                ],
                new \Exception('Exception message'),
                new Error(
                    [
                        'error' =>
                            [
                                'message' => 'Converter message',
                                'code' => 0,
                            ],
                    ],
                    400
                ),
            ],
            'http code from converter' => [
                [
                    ExceptionConverter::create(\Exception::class, httpCode: 400),
                ],
                new \Exception('Exception message'),
                new Error(
                    [
                        'error' =>
                            [
                                'message' => 'Exception message',
                                'code' => 0,
                            ],
                    ],
                    400,
                ),
            ],
            'callable message' => [
                [
                    ExceptionConverter::create(
                        \Exception::class,
                        errorMessage: function (\Exception $exception): array {
                            return ['error' => sprintf('[%s]', $exception->getMessage())];
                        },
                    ),
                ],
                new \Exception('Exception message'),
                new Error(
                    ['error' => '[Exception message]'],
                    400,
                ),
            ],
            'match by class' => [
                [
                    ExceptionConverter::create(DummyException::class),
                ],
                new DummyException('Exception message', 1),
                new Error(
                    [
                        'error' =>
                            [
                                'message' => 'Exception message',
                                'code' => 1,
                            ],

                    ],
                    400,
                ),
            ],
            'match by namespace' => [
                [
                    ExceptionConverter::create('Grifix\ErrorPresenter'),
                ],
                new DummyException('Exception message', 1),
                new Error(
                    [
                        'error' =>
                            [
                                'message' => 'Exception message',
                                'code' => 1,
                            ],

                    ],
                    400,
                ),
            ],
            'match by regexp' => [
                [
                    ExceptionConverter::create('/Dummy/'),
                ],
                new DummyException('Exception message', 1),
                new Error(
                    [
                        'error' =>
                            [
                                'message' => 'Exception message',
                                'code' => 1,
                            ],

                    ],
                    400,
                ),
            ],
            'not match' => [
                [
                    ExceptionConverter::create('/Fake/'),
                ],
                new DummyException('Exception message', 1),
                new Error(
                    [
                        'error' =>
                            [
                                'message' => 'Server error',
                                'code' => 0,
                            ],

                    ],
                    500,
                ),
            ],
            'defined error code' => [
                [
                    ExceptionConverter::create(DummyException::class, null, 5)
                ],
                new DummyException('Exception message', 1),
                new Error(
                    [
                        'error' =>
                            [
                                'message' => 'Exception message',
                                'code' => 5,
                            ],

                    ],
                    400,
                ),
            ],
        ];
    }
}

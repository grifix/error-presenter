<?php

declare(strict_types=1);

namespace Grifix\ErrorPresenter\Tests;

final class DummyException extends \Exception
{
}
